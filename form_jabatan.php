
<form method="POST" action="controllerJabatan.php">
  <div class="form-group row">
    <label for="jabatan" class="col-5 col-form-label">Jabatan</label> 
    <div class="col-7">
      <input id="jabatan" name="jabatan" placeholder="New Jabatan" type="text" class="form-control" required="required">
    </div>
  </div> 
  <div class="form-group row">
    <div class="offset-5 col-7">
      <button name="submit" type="submit" class="btn btn-primary">Simpan</button>
    </div>
  </div>
</form>