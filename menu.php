<div class="row">
		<div class="col-md-12">
			<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
				 
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
					<span class="navbar-toggler-icon"></span>
				</button> <a href="index.php?hal=home">Home</a>
				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					<ul class="navbar-nav">
						<li class="nav-item dropdown">
							 <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown">Master Data</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								 <a class="dropdown-item" href="index.php?hal=karyawan">Data Karyawan</a>
								 <a class="dropdown-item" href="index.php?hal=user">Data User</a>
								 <a class="dropdown-item" href="index.php?hal=divisi">Data Divisi</a>
								 <a class="dropdown-item" href="index.php?hal=jabatan">Data Jabatan</a>
								 <a class="dropdown-item" href="index.php?hal=jenis">Jenis Cuti</a>
							</div>
						</li>
						<li class="nav-item dropdown">
							 <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown">Form</a>
							<div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
								 <a class="dropdown-item" href="index.php?hal=cuti">Form Cuti</a>
								 <a class="dropdown-item" href="index.php?hal=izin">Form Izin</a>
								 <a class="dropdown-item" href="index.php?hal=sakit">Form Sakit</a>
							</div>
						</li>
					</ul>
					<ul class="navbar-nav ml-md-auto">
						<form class="form-inline">
						<input class="form-control mr-sm-2" type="text" placeholder="Cari Data Karyawan" > 
						<button class="btn btn-primary my-2 my-sm-0" type="submit">
							Search
						</button>
						&nbsp;&nbsp;
						<button class="btn btn-success my-2 my-sm-0" type="submit">
							Logout
						</button>
					</form>
					</ul>
				</div>
			</nav>
		</div>
	</div>