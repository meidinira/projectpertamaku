<?php
include_once 'koneksi.php';
include_once 'KaryawanModel.php';

//tangkap request form nama nama yang ada di element form
$nik = $_POST ['nik'];
$nama = $_POST ['nama'];
$jk = $_POST ['jk'];
$jabatan = $_POST ['jabatan'];
$divisi = $_POST ['divisi'];
$agama = $_POST ['agama'];
$status = $_POST ['status'];
$jumlah = $_POST ['jumlah_anak'];
$hp = $_POST ['hp'];
$email = $_POST['email'];
$alamat = $_POST ['alamat'];
$foto = $_POST ['foto'];
$tgl = $_POST ['tgl_masuk'];

//gabungkan var di atas ke array
$data = [
	$nik,
	$nama,
	$jk,
	$jabatan,
	$divisi,
	$agama,
	$status,
	$jumlah,
	$hp,
	$email,
	$alamat,
	$foto,
	$tgl

];

//panggil fungsi simpan di PegawaiModel.php

$model = new KaryawanModel();
$model->simpan($data);

// landing page kehalaman pegawai

header('location:index.php?hal=karyawan');

?>